#include<stdio.h>
#include<stdlib.h>
#include<string>
#include<string.h>
#include <iostream>
#include "ImageManip.h"
#include "FiltroNegativo.h"
#include "FiltroEscalaCinza.h"
#include "FiltroPolarizado.h"

using namespace std;

int main(){

  string iFileName;
  imageFile *image;
  FiltroEscalaCinza FiltroEC;
  FiltroNegativo  FiltroNeg;
  FiltroPolarizado FiltroPol;
while(true){
    int opcao, opcaofiltro;


    cout << "############################################" << endl;
    cout << "Aplicacao de Filtros em Imagens. Escolha uma opcao:" << endl;
    cout << "1. Carregar imagem ao Buffer" << endl;
    cout << "2. Aplicar Filtro" << endl;
    cout << "3. Salvar Imagem do Buffer como output.ppm" << endl;
    cout << "4. Sair" << endl;

    cout << "#############################################" << endl;
    cin >> opcao;

    switch (opcao) {
      case 1:
        cout << "Digite o nome completo da imagem a ser carregada ao buffer" << endl;
        cin >> iFileName;
        image =  FiltroNeg.readPPM((iFileName).c_str());
        cout << "A imagem foi salva no buffer!" << endl;
        break;

      case 2:
        cout << endl << "Escolha um filtro a ser aplicado:" << endl;
        cout << "1. Filtro Negativo" << endl;
        cout << "2. Filtro Preto e Branco" << endl;
        cout << "3. Filtro Polarizado" << endl;
        cout << "3. Filtro de Medias" << endl;
        cin >> opcaofiltro;
        switch (opcaofiltro) {
          case 1:
            image = FiltroNeg.AplicarFiltro(image);
            cout << "O filtro foi aplicado na imagem!" << endl;
            break;
          case 2:
            image = FiltroEC.AplicarFiltro(image);
            cout << "O filtro foi aplicado na imagem!" << endl;
            break;
          case 3:
            image = FiltroPol.AplicarFiltro(image);
            cout << "O filtro foi aplicado na imagem!" << endl;
            break;
          case 4:
            image = FiltroPol.AplicarFiltro(image);
            break;
        }

      case 3:
        FiltroNeg.writePPM("output.ppm",image);
        printf("As dimensoes da imagem sao: %d x %d\n", image->x, image->y );
        printf("A imagem foi salva! Pressione qualquer tecla para continuar...");
        break;
      case 4:
        exit(0);


    }




    printf("\n \n \n");
    getchar();}
}
