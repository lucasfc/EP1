#include "ImageManip.h"
#include<stdio.h>
#include<stdlib.h>
#include<string>
#define COMPONENTE_RGB 255

using namespace std;


ImageManip::ImageManip()
{
    //ctor
}

imageFile *ImageManip::readPPM(const char *filename)
{
         char temp[16];
         imageFile *img;
         FILE *fp;
         int rgb_valor;
         fp = fopen(filename, "rb");



    if (!fp) {
              fprintf(stderr, "Nao foi possivel encontrar o arquivo '%s'\n", filename);
              exit(1);
    }

         //verifica formato do arquivo
         if (!fgets(temp, sizeof(temp), fp)) {
              perror(filename);
              exit(1);
         }

    //verifica formato da imagem
    if (temp[0] != 'P' || temp[1] != '6') {
         fprintf(stderr, "Formato invalido\n");
         exit(1);
    }

    //alocacao de memoria da imagem
    img = (imageFile *)malloc(sizeof(imageFile));
    if (!img) {
         fprintf(stderr, "Falha na alocacao de memoria!\n");
         exit(1);
    }


    //leitura do tamanho da imagem
    if (fscanf(fp, "%d %d", &img->x, &img->y) != 2) {
         fprintf(stderr, "Tamanho de imagem invalido!\n");
         exit(1);
    }

    //leitura do componente rgb
    if (fscanf(fp, "%d", &rgb_valor) != 1) {
         fprintf(stderr, "Componente RGB invalido!\n");
         exit(1);
    }

    //leitura dos valores rgb
    if (rgb_valor!= COMPONENTE_RGB) {
         fprintf(stderr, "Componente RGB excede o valor esperado!\n");
         exit(1);
    }

    while (fgetc(fp) != '\n') ;
    //alocacao de memoria
    img->data = (pixelData*)malloc(img->x * img->y * sizeof(pixelData));

    if (!img) {
         fprintf(stderr, "Falha na alocacao de memoria!\n");
         exit(1);
    }

    //ler pixels do arquivo
    if (fread(img->data, 3 * img->x, img->y, fp) != img->y) {
         fprintf(stderr, "Erro ao carregar arquivo!\n");
         exit(1);
    }

    fclose(fp);
    return img;
}
void ImageManip::writePPM(const char *filename, imageFile *img)
{
    //criacao do output
    FILE *fp;
    fp = fopen(filename, "wb");
    if (!fp) {
         fprintf(stderr, "Arquivo nao pode ser aberto!\n");
         exit(1);
    }

    fprintf(fp, "P6\n");
    fprintf(fp, "%d %d\n",img->x,img->y);
    fprintf(fp, "%d\n",COMPONENTE_RGB);
    fwrite(img->data, 3 * img->x, img->y, fp);
    fclose(fp);
}
