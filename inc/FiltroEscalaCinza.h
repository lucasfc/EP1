#ifndef FiltroEscalaCinza_H
#define FiltroEscalaCinza_H

class FiltroEscalaCinza : public ImageManip
{
    public:

      int temp, filterRes, imageWidth, imageHeight;
      FiltroEscalaCinza();
      imageFile * AplicarFiltro(imageFile *img);
    protected:

    private:
    unsigned char   *p;
};


#endif //FiltroEscalaCinza_H
