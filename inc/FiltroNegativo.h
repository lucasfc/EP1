#ifndef FILTRONEGATIVO_H
#define FILTRONEGATIVO_H

class FiltroNegativo : public ImageManip
{
    public:

      int temp, filterRes, imageWidth, imageHeight;
      FiltroNegativo();
      imageFile * AplicarFiltro(imageFile *img);
    protected:

    private:
    unsigned char   *p;
};


#endif //FILTRONEGATIVO_H
