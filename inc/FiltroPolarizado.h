#ifndef FiltroPolarizado_H
#define FiltroPolarizado_H

class FiltroPolarizado : public ImageManip
{
    public:

      int temp, filterRes, imageWidth, imageHeight;
      FiltroPolarizado();
      imageFile * AplicarFiltro(imageFile *img);
    protected:

    private:
    unsigned char   *p;
};


#endif //FiltroPolarizado_H
