#ifndef IMAGEMANIP_H
#define IMAGEMANIP_H

typedef struct {
     unsigned char red,green,blue;
} pixelData;


typedef struct {
     int x, y;
     pixelData *data;
} imageFile;


class ImageManip
{
    public:
         char temp[16];
         int rgb_valor;
        ImageManip();
        static imageFile * readPPM(const char *filename);
        void writePPM(const char *filename, imageFile *img);
    protected:

    private:
};

#endif // IMAGEMANIP_H
