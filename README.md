# EP1 - OO (UnB - Gama)

Este projeto consiste em um programa em C++ capaz de aplicar filtros em imagens de formato `.ppm`.

O programa é utilizado pelo seu binário, acessado pelo terminal. Para compilar e executar o programa, no diretório raiz do repositório são usados os comandos:

$make  : Para compilar o programa.

$make run : Para executar o programa após compilado

$make clean : Remove os objetos criados. Deleta a imagem “output.ppm” criada.


Ao iniciar o programa, é apresentada uma série de opções ao usuário, explicadas a seguir:

#### 1. Carregar imagem ao Buffer.
Essa opção carrega uma imagem ao buffer do programa, permitindo que ela seja modificada. 

O usuário receberá um pedido para digitar o nome da imagem a ser carregada ao buffer. 

O nome da imagem deve incluir a sua extensão.

#### 2. Aplicar filtro à imagem
Após carregada, o programa permite que o usuário aplique filtros pré-definidos à imagem no buffer.

Os filtros disponíveis atualmente são: Negativo, Escala de Cinza, Polarizado e Filtro de Médias.

Mais de um filtro pode ser aplicado em cada imagem.

#### 3. Salvar imagem como "output.ppm"
Essa opção salva a imagem com o nome descrito na raiz do diretório do programa.

#### 4. Sair
Essa opção encerra o programa e remove a imagem do buffer.